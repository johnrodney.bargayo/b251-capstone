from abc import ABC, abstractmethod

class Person(ABC):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def getFullName(self):
        return self._firstName + ' ' + self._lastName

   
    def addRequest(self):
        pass

    
    def checkRequest(self):
        pass

    
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName, lastName, email, department)
        self._requests = []

    def addRequest(self, request):
        self._requests.append(request)
        return 'Request has been added'

   
    def firstName(self):
        return self._firstName
    
    def firstName(self, firstName):
        self._firstName = firstName

    
    def lastName(self):
        return self._lastName
   
    def lastName(self, lastName):
        self._lastName = lastName

   
    def email(self):
        return self._email
    
    def email(self, email):
        self._email = email

   
    def department(self):
        return self._department
    
    def department(self, department):
        self._department = department

    def checkRequest(self):
        return 'Placeholder method'
    def addUser(self):
        return 'Placeholder method'
    def login(self):
        return self._email + ' has logged in'
    def logout(self):
        return self._email + ' has logged out'

class TeamLead(Employee):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName, lastName, email, department)
        self._members = []

    def addMember(self, employee):
        self._members.append(employee)
        return 'Added employee to the members list'
        
    def get_members(self):
        return self._members

class Admin(Employee):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName, lastName, email, department)
        self._users = []

    def addUser(self, user):
        self._users.append(user)
        return 'User has been added'

class Request():
    def __init__(self, name, requester, dateRequested, status):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = status

    def set_status(self, status):
        self._status = status


   
    def name(self):
        return self._name
    
    def name(self, name):
        self._name = name

    
    def requester(self):
        return self._requester

    def requester(self, requester):
        self._requester = requester

   
    def dateRequested(self):
        return self._dateRequested
   
    def dateRequested(self, dateRequested):
        self._dateRequested = dateRequested


    def status(self):
        return self._status
  
    def status(self, status):
        self._status = status

    def updateRequest(self, name, requester, dateRequested, status):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = status
        return 'Request updated'

    def closeRequest(self):
        self._status = 'Closed'
        return 'Request closed'

    def cancelRequest(self):
        self._status = 'Cancelled'
        return 'Request cancelled'

# Test Cases:

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandin@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021", "Open")
req2 = Request("Laptop repair", employee1, "1-Jul-2021", "Open")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest(req2) == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())


assert admin1.addUser(employee1) == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())

print(employee1.login())
admin1.addUser(employee1)